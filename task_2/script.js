
var apiUrl = "https://cors-anywhere.herokuapp.com/https://api.sportradar.us/soccer-t3/eu/en/schedules/live/results.json?api_key=g44gynsab3nc69xwr8cgzujh"
function ajax_get(url, callback) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            console.log('responseText:' + xmlhttp.responseText);
            try {
                var data = JSON.parse(xmlhttp.responseText);
            } catch(err) {
                console.log(err.message + " in " + xmlhttp.responseText);
                return;
            }
            callback(data);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}
function getResult(){
ajax_get(apiUrl, function(data) {
     
    var html = "<h2>Results</h2>";
    //html += "<h3>" + data["description"] + "</h3>";
    html += "<table>";
       for (var i=0; i < data["results"].length; i++) {
        html += "<tr>";
           html += '<td>' + data["results"][i].sport_event["competitors"][0].name + "</td>"
           + '<td>' +data["results"][i].sport_event_status["home_score"] + " - " + data["results"][i].sport_event_status["away_score"] + "</td>"
           + '<td class="rigth">' + data["results"][i].sport_event["competitors"][1].name + "</td>";
       }
    html += "</table>";
    document.getElementById("text").innerHTML = html;
});
}